@extends('layouts.admin_layout')

@section('title', 'Изображения')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Изображения</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i>{{ session('success') }}</h4>
                </div>
            @endif
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-body p-0">
                    <table class="table table-striped projects">
                        <thead>
                        <tr>
                            <th>
                                Заголовок
                            </th>
                            <th>
                                Описание
                            </th>
                            <th>
                                Изображения
                            </th>
                            <th>
                                Категория
                            </th>
                            <th>
                                Тэги
                            </th>
                            <th style="width: 30%">
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    {{ $image['title'] }}
                                </td>
                                <td>
                                    {{ $image['text'] }}
                                </td>
                                <td>
                                    <img src="{{ Storage::url($image->img)}}" style="height: 240px">
                                </td>
                                <td>
                                    {{ optional($image->category)->name }}
                                </td>
                                <td>
                                    {{ $image['tag'] }}
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
