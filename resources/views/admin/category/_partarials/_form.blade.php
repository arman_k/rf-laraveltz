<form action="{{ route('category.destroy', $category['id']) }}" method="POST"
      style="display: inline-block">
    @csrf
    @method('DELETE')
    <button type="submit" class="btn btn-danger btn-sm delete-btn">
        <i class="fas fa-trash">
        </i>
        Удалить
    </button>
</form>
